package mi_hw1;

import java.util.Comparator;

public class MyAreaComparator implements Comparator<Vehicle>{
	@Override
	public int compare(Vehicle v2, Vehicle v1) {
		
		return Integer.compare((v1.length * v1.width), (v2.length * v2.width) );
	}
}
