package mi_hw1;

import java.util.Comparator;

public class MyComparator implements Comparator<Vehicle>{
	@Override
	public int compare(Vehicle v1, Vehicle v2) {
		
		return Integer.compare(v2.length, v1.length);
	}

}
