package mi_hw1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;

public class Main {
	public static void main(String[] args) throws IOException {
		InputStreamReader isr = new InputStreamReader(System.in);
		BufferedReader br = new BufferedReader(isr);
		String buffer;
		
		int parkinglotLength = 0;
		int parkinglotWidth = 0;
		
		buffer = br.readLine();
		String[] parkinglotSize = buffer.split("\t");
		parkinglotLength = Integer.parseInt(parkinglotSize[0]);
		parkinglotWidth = Integer.parseInt(parkinglotSize[1]);
		
		int numberOfCars = Integer.parseInt(br.readLine());
		
		ArrayList<Vehicle> Vehicles = new ArrayList<Vehicle>();
		int id = 1;
		while(id <= numberOfCars) {
			buffer = br.readLine();
			String[] vehicleSize = buffer.split("\t");
			if( Integer.parseInt(vehicleSize[0]) > Integer.parseInt(vehicleSize[1]) ) {
				Vehicles.add(new Vehicle(id++, Integer.parseInt(vehicleSize[0]), Integer.parseInt(vehicleSize[1])));
			}
			else {
				Vehicles.add(new Vehicle(id++, Integer.parseInt(vehicleSize[1]), Integer.parseInt(vehicleSize[0])));
			}
		}
		
		Collections.sort(Vehicles, new MyAreaComparator());
				
		int[][] parkinglot = new int[parkinglotLength][parkinglotWidth];
		
		int listIterator = 0;
		while(listIterator < numberOfCars) {
			parkinglot = myParkingSort(parkinglot, parkinglotLength, parkinglotWidth, listIterator, Vehicles);
			listIterator++;
		}
		if( !isSorted(parkinglot, parkinglotLength, parkinglotWidth) ) {
			for(int i = 0; i < parkinglotLength; i++) {
				for(int j = 0; j < parkinglotWidth; j++) {
						parkinglot[i][j] = 0 ;
				}
			}
			
			listIterator = 0;
			while(listIterator < numberOfCars) {
				parkinglot = myParkingSortAltered(parkinglot, parkinglotLength, parkinglotWidth, listIterator, Vehicles);
				listIterator++;
			}
		}
		if( !isSorted(parkinglot, parkinglotLength, parkinglotWidth)) {
			for(int i = 0; i < parkinglotLength; i++) {
				for(int j = 0; j < parkinglotWidth; j++) {
						parkinglot[i][j] = 0 ;
				}
			}
			Collections.sort(Vehicles, new MyComparator());
			
			listIterator = 0;
			while(listIterator < numberOfCars) {
				parkinglot = myParkingSort(parkinglot, parkinglotLength, parkinglotWidth, listIterator, Vehicles);
				listIterator++;
			}
		}
		
		if( !isSorted(parkinglot, parkinglotLength, parkinglotWidth)) {
			for(int i = 0; i < parkinglotLength; i++) {
				for(int j = 0; j < parkinglotWidth; j++) {
						parkinglot[i][j] = 0 ;
				}
			}
			Collections.sort(Vehicles, new MyComparator());
			
			listIterator = 0;
			while(listIterator < numberOfCars) {
				parkinglot = myParkingSortAltered(parkinglot, parkinglotLength, parkinglotWidth, listIterator, Vehicles);
				listIterator++;
			}
		}
		
		myPrint(parkinglot, parkinglotLength, parkinglotWidth);
			
	}
	
	static boolean isSorted(int parkinglot[][], int parkinglotLength, int parkinglotWidth) {
		boolean Sorted = true;
		for(int i = 0; i < parkinglotLength; i++) {
			for(int j = 0; j < parkinglotWidth; j++) {
				if(parkinglot[i][j] == 0) {
				Sorted = false;
				}
			}
		}
		return Sorted;
	}
	
	static void myPrint(int[][] parkinglot, int parkinglotLength, int parkinglotWidth) {
		
		for(int i = 0; i < parkinglotLength; i++) {
			for(int j = 0; j < parkinglotWidth; j++) {
				if(j != parkinglotWidth - 1) { 
					System.out.printf("%d\t", parkinglot[i][j]);
				}
				else { 
					System.out.printf("%d", parkinglot[i][j]);
				}
			}
			System.out.println();
		}
		
	}
	static int[][] myParkingSort(int[][] parkinglot, int parkinglotLength, int parkinglotWidth, int listIterator, ArrayList<Vehicle> Vehicles){
		for(int i = 0; i < parkinglotLength; i++) {
			for(int j = 0; j < parkinglotWidth; j++) {
				if( parkinglot[i][j] == 0 ) {
					if((i + Vehicles.get(listIterator).width) <= parkinglotWidth && (j + Vehicles.get(listIterator).length) <= parkinglotLength) {
						boolean isFree = true;
						for(int k = i; k < Vehicles.get(listIterator).width + i; k++) {
							for(int l = j; l < Vehicles.get(listIterator).length + j; l++) {
								if(parkinglot[k][l] != 0) {
									isFree = false;
								}
							}
						}
						
						if( isFree ) {
							for(int k = i; k < Vehicles.get(listIterator).width + i; k++) {
								for(int l = j; l < Vehicles.get(listIterator).length + j; l++) {
									parkinglot[k][l] = Vehicles.get(listIterator).id;
								}
							}
							return parkinglot;							
						}
					}
					else if((i + Vehicles.get(listIterator).length) <= parkinglotLength && (j + Vehicles.get(listIterator).width) <= parkinglotWidth) {
						boolean isFree = true;
						for(int k = i; k < Vehicles.get(listIterator).length + i; k++) {
							for(int l = j; l < Vehicles.get(listIterator).width + j; l++) {
								if(parkinglot[k][l] != 0) {
									isFree = false;
								}
							}
						}
						
						if( isFree ) {
							for(int k = i; k < Vehicles.get(listIterator).length + i; k++) {
								for(int l = j; l < Vehicles.get(listIterator).width + j; l++) {
									parkinglot[k][l] = Vehicles.get(listIterator).id;
								}
							}
							
							return parkinglot;							
						}
					}
					
				}
			}
		}
		return parkinglot;
	}
	
	static int[][] myParkingSortAltered(int[][] parkinglot, int parkinglotLength, int parkinglotWidth, int listIterator, ArrayList<Vehicle> Vehicles){
		for(int i = 0; i < parkinglotLength; i++) {
			for(int j = 0; j < parkinglotWidth; j++) {
				if( parkinglot[i][j] == 0 ) {
					if((i + Vehicles.get(listIterator).length) <= parkinglotLength && (j + Vehicles.get(listIterator).width) <= parkinglotWidth) {
						boolean isFree = true;
						for(int k = i; k < Vehicles.get(listIterator).length + i; k++) {
							for(int l = j; l < Vehicles.get(listIterator).width + j; l++) {
								if(parkinglot[k][l] != 0) {
									isFree = false;
								}
							}
						}
						
						if( isFree ) {
							for(int k = i; k < Vehicles.get(listIterator).length + i; k++) {
								for(int l = j; l < Vehicles.get(listIterator).width + j; l++) {
									parkinglot[k][l] = Vehicles.get(listIterator).id;
								}
							}
							
							return parkinglot;							
						}
					}
					
					else if((i + Vehicles.get(listIterator).width) <= parkinglotWidth && (j + Vehicles.get(listIterator).length) <= parkinglotLength) {
							boolean isFree = true;
							for(int k = i; k < Vehicles.get(listIterator).width + i; k++) {
								for(int l = j; l < Vehicles.get(listIterator).length + j; l++) {
									if(parkinglot[k][l] != 0) {
										isFree = false;
									}
								}
							}
							
							if( isFree ) {
								for(int k = i; k < Vehicles.get(listIterator).width + i; k++) {
									for(int l = j; l < Vehicles.get(listIterator).length + j; l++) {
										parkinglot[k][l] = Vehicles.get(listIterator).id;
									}
								}
								return parkinglot;							
							}
						}
					
				}
			}
		}
		return parkinglot;
	}
}