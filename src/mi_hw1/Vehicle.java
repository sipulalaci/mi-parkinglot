package mi_hw1;

public class Vehicle {
	protected int id;
	protected int length;
	protected int width;
	
	Vehicle( int i, int l, int w){
		id = i;
		length = l;
		width = w;
	}
	
	public String toString() {
		return id + " " + length + " " + width;
	}
}
 